module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Mesure', {
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        poids: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate:{
                isInt : { msg: "Utiliser des nombres entier pour votre poids"},
                notNull: {msg: "Votre poids est requis"},
            }
        }},
    );
};