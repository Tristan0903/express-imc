const Mesure = require('../config/sequelize');
const {ValidationError} = require("sequelize");
const { success } = require("../helpers");




exports.findAllMesures = (req,res) => {
    Mesure.findAll().then((mesure) => {
        const message = "La liste des mesures a bien été récuprée.";
        res.json(success(message,mesure));
    });
};


exports.createMesure = (req,res) => {
    Mesure.create(req.body).then((mesure) => {
        const message = `La mesure de ${req.body.name} a bien été prise.`;
        res.json(success(message, mesure));
    }).catch(error=>{
        if (error instanceof ValidationError) {
            return res.status(400).json({message: error.message, data: error});
        }
        const message = "La mesure n'a pas pu être prise. Réessayer dans quelques instants...";
        return res.status(500).json({message: message, data: error});
    });
};

exports.findByNameMesures = (req,res) => {
    const name = req.params.name;
    Mesure.findAll({where: {name:name},
    }).then((mesure) => {
        const message = `Les mesures de ${req.params.name} ont bien été trouvées.`;
        res.json(success(message, mesure));
    });
};

exports.destroyMesure = (req,res) => {
    Mesure.findByPk(req.params.id).then(mesure => {
        if (mesure === null) {
            const message = "Cette mesure n'existe pas. Réessayer dans quelques instants...";
            return res.status(404).json(message);
        }
        const mesureDeleted = mesure;
        Mesure.destroy({
            where: {id: mesure.id},
        }).then((_) => {
            const message = `La mesure ${req.params.id} de ${req.body.name} a été supprimée.`;
            res.json(success(message, mesureDeleted));
        });
    });
};