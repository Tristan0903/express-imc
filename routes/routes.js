const express = require("express");
const router = express.Router()

const { findByNameMesures, createMesure, findAllMesures, destroyMesure } = require("../controllers/controllerMesure");
const { findAllUsers, createUser, findByNameUser, register, login, profile } = require("../controllers/controllerUser");
const checkUpName = require("../middleware/verifyRegister");
const verifyTokens  = require("../middleware/auth.jwt");

// pour les mesures du poids
router.get("/mesures",findAllMesures);
router.post("/mesures",createMesure);
router.get("/mesures/:name",findByNameMesures);
router.delete("/mesures/:id", destroyMesure);

// pour les users
router.get("/users",findAllUsers);
router.post("/users",createUser);
router.get("/users/:name",findByNameUser);


// pour le token
router.post("/register", checkUpName, register);
router.post("/login", login);
router.get("/profile", verifyTokens, profile);

module.exports = router;