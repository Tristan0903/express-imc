module.exports = (sequelize, Datatypes) => {
    return sequelize .define('User',{
        id: {
            type : Datatypes.INTEGER,
            primaryKey : true,
            autoIncrement : true,
        },
        name: {
            type : Datatypes.STRING,
            unique : { msg: "Ce nom est déjà pris" },
            allowNull : false,
        },
        password: {
            type : Datatypes.STRING,
            allowNull : false,
        },
        age: {
            type : Datatypes.INTEGER,
            allowNull : false,
        },
        taille: {
            type : Datatypes.INTEGER,
            allowNull : false,
        },
        poids: {
            type : Datatypes.INTEGER,
            allowNull : false,
        },
    });
};