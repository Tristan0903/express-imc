const User = require('../config/sequelize');
const {ValidationError} = require("sequelize");
const { success } = require("../helpers");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");




exports.findAllUsers = (req,res) => {
    User.findAll().then((user) => {
        const message = "La liste des utilisateurs a bien été récupérée.";
        res.json(success(message,user));
    });
};


exports.createUser = (req,res) => {
    User.create(req.body).then((user) => {
        const message = `L'utilisateur ${req.body.name} a bien été crée.`;
        res.json(success(message,user));
    }).catch(error=>{
        if (error instanceof ValidationError) {
            return res.statut(400).json({message: error.message, data: error});
        }
        const message = "L'utilisateur n'a pas pu être crée. Réessayer dans quelques instants...";
        return res.statut(500).json({message: message, data: error});
    });
};

exports.findByNameUser = (req,res) => {
    const name = req.params.name;
    console.log(req.params.name);
    User.findAll({where: {name:name},
    }).then((user) => {
        const message = `L'utilisateur ${req.params.name} est vérifié.`;
        res.json(success(message,user));
    });
};



// pour le token


exports.register = (req, res, next) => {
    try {
      const { name, age, password, taille, poids } = req.body;
      bcrypt.hash(password, 10).then((psd) => {
        User.create({
          name: name,
          password: psd,
          age: age,
          taille: taille,
          poids: poids,
        }).then((user) => {
          res.send({ message: "Utilisateur enregistré", data: user });
        });
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  };
  
  exports.login = (req, res, next) => {
    try {
      let { name, password } = req.body;
  
      User.findOne({
        where: {
          name: name,
        },
      }).then((user) => {
        if (!user) {
          return res.status(400).send({ message: "User not found." });
        }
  
        var passwordIsValid = bcrypt.compareSync(password, user.password);
  
        if (!passwordIsValid) {
          return res
            .status(401)
            .send({ token: null, message: "Invalid Password" });
        }
  
        var token = jwt.sign({ id: user.id }, config.secret, {
          expiresIn: 86400,
        });
  
        res.send({
          data: {
            id: user.id,
            name: user.name,
            token: token,
          },
          status: 200,
        });
      });
    } catch (error) {
      res.status(500).send({ error: error.message });
    }
  };
  
  exports.profile = (req, res, next) => {
    res.send({
      status: 1,
      data: { name: "C'est moi" },
      message: "Successful",
    });
    next();
  };