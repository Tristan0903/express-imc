const { Sequelize, DataTypes } = require("sequelize");

const MesureModel = require("../models/mesureModel");
const UserModel = require('../models/user');

const sequelize = new Sequelize("imc", "root", "", {
  host: "localhost",
  dialect: "mariadb",
  logging: false,
});

const Mesure = MesureModel(sequelize, DataTypes);
const User = UserModel(sequelize,DataTypes);

module.exports = Mesure;
module.exports = User;
