const express = require("express");
const indexRouter = require("./routes/routes");
const cors = require("cors");
const sequelize = require("./config/sequelize");

const port = 3000;

const app = express();
app.use(cors());
app.use(express.json());

app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "Authorization, Origin, Content-Type, Accept"
    );
    next();
  });

app.use("/api", indexRouter);
 //sequelize.sync();

app.listen(port, ()=>
    console.log(`Votre application est démarrée sur : http://localhost:${port}`)
);